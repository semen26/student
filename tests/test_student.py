# pylint: disable=missing-docstring
import pytest

from src.student import get_logins


@pytest.fixture()
def file_student(tmpdir):
    filename = "data.txt"
    filename = tmpdir.join(filename)
    with open(filename, "w") as file_:
        file_.write("""Иванов Иван Иванович  ;  anetto-
Петров Иван Иванович;anetto-
""")
    return filename

@pytest.fixture()
def file_student_wrong(tmpdir):
    filename = "data_wrong.txt"
    filename = tmpdir.join(filename)
    with open(filename, "w") as file_:
        file_.write("""Иванов Иван Иванович  ;  anetto-
Петров Иван Иванович;anetto-2
""")
    return filename


def test_student(file_student):
    sh_ans = [{'fio': 'Иванов Иван Иванович', 'login': 'anetto-'},
              {'fio': 'Петров Иван Иванович', 'login': 'anetto-'}]
    ans = get_logins(file_student)

    assert ans == sh_ans

def test_student_wrong(file_student_wrong):
    with pytest.raises(RuntimeError):
        ans = get_logins(file_student_wrong)

a = 3